/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

int main(int agrc,char* argv[])
{
	cout << "LILI Example String" << endl << endl;

	LILIString	text1("hallo");
	cout << text1 << endl;
	
	LILIString text2(text1);
	cout << text2 << endl;

	LILIString text3;
	cout << text3 << endl;

	text2 = "Hello";
	cout << text2 << endl;
	
	text2 += text1;
	cout << text2 << endl;
	
	text2 = text2 + text1;
	cout << text2 << endl;

	text2 = text2 + "Hello2";
	cout << text2 << endl;

	cout << "Equal: " << text2.equal(text1) << endl;
	
	text1 = "HellohallohalloHello2";
	cout << "Equal: " << text2.equal(text1) << endl;

	cout << "Find o: " << text2.find('o') << endl;	

	cout << "Find hallo: " << text2.find("hallo") << endl;	

	cout << "Find last o: " << text2.findLastOf('o') << endl;	

	cout << "Substr: " << text2.substr(5,5) << endl;	

	text2.clear();
	text2.append("Hello world");
	
	cout << text2 << endl;

	return 1;
}

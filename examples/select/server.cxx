/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

int main(int argc,char* argv[])
{
	LILIBytes data;
	LILISocket *socket = 0;
	
	cout << "LILI Example Server select" << endl << endl;

	LILISocket server;
	
	server.initServer(2000);
	
	lililog.log(LILILog::info,"Listening on port 2000");

	LILISelect select;
	
	select.addFd(server.getHandle());
	
	while(1) {
		select.select(5000);
		
		int fd = select.first();
		
		if(fd==-1) {
			lililog.log(LILILog::info,"Server timout after 5 sec");
			break;
		}
		
		while(fd!=-1) {
			if(fd == server.getHandle()) {
				socket = server.accept();
				lililog.log(LILILog::info,"Client connected");
				select.addFd(socket->getHandle());
			}
			else if(socket && (fd == socket->getHandle())) {
				if(socket->recv(data)==LILI_ERROR) {
					lililog.log(LILILog::info,"Client disconnected");
					select.removeFd(socket->getHandle());
					socket->close();
					delete socket;
					socket = 0;
				}
				else {
					cout << "Recvd: " << LILIString(data) << endl;
				}			
			}
			
			fd = select.next();
		}
	}
	
	if(socket) {
		socket->close();
		delete socket;
	}
	
	server.close();
	
	return 1;
}

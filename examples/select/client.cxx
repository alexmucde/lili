/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

int main(int argc,char* argv[])
{
	LILIString text("Hello world");
	
	cout << "LILI Example Client" << endl << endl;

	LILISocket client;
	
	client.initClient();
	
	client.connect("localhost",2000);
	
	client.send(LILIBytes((unsigned char*)text.ptr(),text.size()));
	
	sleep(1);
	
	client.close();
	
	return 1;
}

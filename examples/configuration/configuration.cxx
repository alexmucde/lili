/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

class ExampleConfiguration : public LILIConfiguration
{
	public:
		ExampleConfiguration()
		{
			conf2 = false;
		}
		
		int parseToken(LILIString token,LILIString value)
		{
			if(token=="Conf1") {
				conf1 = value;
				return LILI_OK;
			}
			else if(token=="Conf2") {
				conf2 = atoi(value);
				return LILI_OK;
			}
			
			return LILI_ERROR;
		}
		
		string conf1;
		bool conf2;
};

int main(int argc,char* argv[])
{
	ExampleConfiguration configuration;

	cout << "LILI Example Configuration" << endl << endl;

	/* parse configuration */
	if(configuration.parse("../configuration/example.conf")!=LILI_OK) {
		return -1;
	}
		
	cout << "Conf1: " << configuration.conf1 << endl;
	cout << "conf2: " << configuration.conf2 << endl;
		
	return 1;
}

/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

int main(int agrc,char* argv[])
{
	unsigned char input1[] = { 1,2,3,4,5 };
	unsigned char input2[] = { 6,7,8,9,10 };
	
	cout << "LILI Example Bytes" << endl << endl;

	LILIBytes data1(input1,sizeof(input1));

	cout << data1.toString() << endl;
	
	data1.append(LILIBytes(input2,sizeof(input2)));

	cout << data1.toString() << endl;
	
	data1.clear();
	data1.resize(20);

	cout << data1.toString() << endl;

	data1.randomise(15);

	cout << data1.toString() << endl;

	data1.setData(0xaa,10);

	cout << data1.toString() << endl;
	
	data1+=LILIBytes(input2,sizeof(input2));

	cout << data1.toString() << endl;
	
	return 1;
}

# Add executable that is built from the source files
add_executable (bytes bytes.cxx ../../src/lili.cxx)

# Make sure the compiler can find all include files
include_directories (../../src) 

# Make sure the linker can find all needed libraries
# rt: clock_gettime()
target_link_libraries(bytes rt)

# Install example application
install (TARGETS bytes
         RUNTIME DESTINATION bin)

# Add executable that is built from the source files
add_executable (hello hello.cxx ../../src/lili.cxx)

# Make sure the compiler can find all include files
include_directories (../../src) 

# Make sure the linker can find all needed libraries
# rt: clock_gettime()
target_link_libraries(hello rt)

# Install example application
install (TARGETS hello
         RUNTIME DESTINATION bin)

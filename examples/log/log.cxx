/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

int main(int argc,char* argv[])
{
	cout << "LILI Example Log" << endl << endl;

	lililog.log(LILILog::info,"We have some nice information in here");
	
	lililog.setLogLevel(LILILog::fatal);
	lililog.log(LILILog::info,"Do we still see this message?");
	
	LILILog mylog;
	
	mylog.log(LILILog::error,"My own log has an error");

	return 1;
}

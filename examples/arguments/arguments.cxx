/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <iostream>

#include <lili.h>

class ExampleArguments : public LILIArguments
{
public:
	ExampleArguments()
	{
		opt1=false;

		opt2="default";
	}

	int parseOption(LILIString &option)
	{
		if(option=="--opt1") {
			opt1 = true;
			return LILI_OK;
		}

		return LILI_ERROR;		
	}

	int parseOptionWithParamater(LILIString &option,LILIString &parameter)
	{
		if(option=="--opt2") {
			opt2 = parameter;
			return LILI_OK;
		}
		
		return LILI_ERROR;
	}

	int parseArgument(LILIString &argument)
	{
		if(argument=="cmd1") {
			cmd = argument;
			return LILI_OK;
		}
		else if(argument=="cmd2") {
			cmd = argument;
			return LILI_OK;
		}
		
		return LILI_ERROR;		
	}

	void printVersion()
	{
		cout << "arguments version 0.0.1" << endl;
	}
	
	void printUsage()
	{
		cout << "usage: arguments [options]" << endl;		
		cout << "       [--version|-v] [--help|-h]" << endl;		
		cout << "       <command>" << endl;		
		cout << endl;		
		cout << "Options:" << endl;
		cout << " --opt1                   Option 1." << endl;
		cout << " --opt2=<file>            Option 2 with filename." << endl;
		cout << endl;		
		cout << "The mostly used example commands are:" << endl;		
		cout << "   cmd1         Command1." << endl;		
		cout << "   cmd2         Command2." << endl;		
	}

	string cmd;
	string opt2;
	bool opt1;
};

int main(int argc,char* argv[])
{
	ExampleArguments arguments;

	/* parse arguments */
	if(arguments.parse(argc,argv)!=LILI_OK) {
		return -1;
	}

	cout << "LILI Example Arguments" << endl << endl;

	if(arguments.cmd == "cmd1" || arguments.cmd == "cmd2") {
		cout << "Option1: " << arguments.opt1 << endl;
		cout << "Option2: " << arguments.opt2 << endl;
		cout << "Command: " << arguments.cmd << endl;	
	}
	else {
		arguments.printUsage();
		return -1;
	}
		
	return 1;
}

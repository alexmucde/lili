/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef LILI_H
#define LILI_H

/**********************************************************************/
// LILI Configuration
//
// Disable some of the following defines, if you do not want to use
// all classes in LILI.
// Not all classes can be disabled. Please consider the dependencies.
/**********************************************************************/

// LILIString
// Dependency: LILI_BYTES
#define LILI_STRING

// LILIBytes
// Dependency: LILI_STRING
#define LILI_BYTES

// LILITime
// Dependency: none
#define LILI_TIME

// LILILog
// Dependency: LILI_TIME
#define LILI_LOG

// LILISelect
// Dependency: none
#define LILI_SELECT

// LILITimer
// Dependency: none
#define LILI_TIMER

// LILISocket
// Dependency: none
#define LILI_SOCKET

// LILIDir
// Dependency: none
#define LILI_DIR

// LILIArguments
// Dependency: LILI_LOG, LILI_TIME
#define LILI_ARGUMENTS

// LILIConfiguration
// Dependency: LILI_LOG, LILI_TIME
#define LILI_CONFIGURATION

/**********************************************************************/
// Includes needed by LILI
/**********************************************************************/

/* used by all */
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* used by LILISocket */
#include <sys/socket.h> 
#include <arpa/inet.h>  
#include <sys/param.h> 
#include <netdb.h> 

/* used by LILIDir */
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

/**********************************************************************/
// Error values of LILI functions
/**********************************************************************/

//! LILI version.
#define LILI_VERSION "v0.0.1"

//! Return values of LILI functions.
#define LILI_ERROR -1
#define LILI_OK		0
#define LILI_EXIT	1
#define LILI_END	2

/**********************************************************************/
// LILIString
/**********************************************************************/

#ifdef LILI_STRING

class LILIBytes;

//! Text string management class. 
/*!
  This class provides mechanisms to manage strings.
*/
class LILIString
{
	public:
	
		//! Default Constructor.
		/*!
		  Create empty string.
		*/
		LILIString();

		//! Constructor to copy a string
		/*!
		  Copy an existing string
		  \param str the string to be copied.
		*/
		LILIString(const char *str);

		//! Constructor to copy a string from byte array
		/*!
		  Copy an existing string
		  \param bytes the string to be copied.
		  \param offset start copy from offset byte.
		*/
		LILIString(const LILIBytes &bytes,int offset = 0);

		//! Copy constructor
		/*!
		  Copy an existing string
		  \param array string to be copied.
		*/
		LILIString(const LILIString &other);

		//! Destructor.
		/*!
		  This frees the string from memory.
		*/
		~LILIString();

		//! Clear the string.
		void clear();

		//! Get length of string .
		/*!
		  Length is without terminating zero.
		  \return the length of the string
		*/
		int size() const;

		//! Check if string is empty.
		/*!
		  Check if the string is empty.
		  \return true if string is empty, else false
		*/
		bool empty() const;

		//! Return pointer to string in memory.
		/*!
		  \return pointer to string in memory
		*/
		char* ptr() const;

		//! Append string.
		/*!
		  \param other the string to be appended.
		  \return pointer to itself
		*/
		LILIString& append(const char *str);

		//! Append string.
		/*!
		  \param other the string to be appended.
		  \return pointer to itself
		*/
		LILIString& append(const LILIString &other);

		//! Append operator.
		/*!
		  \param other the string to be appended.
		  \return new string object
		*/
		LILIString operator+(const char *str);

		//! Append operator.
		/*!
		  \param other the string to be appended.
		  \return new string object
		*/
		LILIString operator+(const LILIString &other);

		//! Copy operator.
		/*!
		  \param str the string to be appended.
		  \return pointer to itself
		*/
		LILIString& operator+=(const char *str);

		//! Append operator.
		/*!
		  \param other the string to be appended.
		  \return pointer to itself
		*/
		LILIString& operator+=(const LILIString &other);

		//! Copy string.
		/*!
		  \param str the string to be copied.
		  \return pointer to itself
		*/
		LILIString& operator=(const char *str);

		//! Copy string.
		/*!
		  \param other the string to be copied.
		  \return pointer to itself
		*/
		LILIString& operator=(const LILIString &other);

		//! Get pointer to string in memory.
		/*!
		  \return pointer to string in memory
		*/
		operator char*() const;
		
		//! Compare with other string.
		/*!
		  \param str the string to be copied.
		  \return true if they are equal, false if not
		*/
		bool equal(const char *str) const;

		//! Compare with other string.
		/*!
		  \param str the string to be copied.
		  \return false if they are equal, true if not
		*/
		bool operator!=(const char *str) const;

		//! Compare with other string.
		/*!
		  \param str the string to be copied.
		  \return true if they are equal, false if not
		*/
		bool operator==(const char *str) const;

		//! Compare with other string.
		/*!
		  \param other the string to be compared.
		  \return false if they are equal, true if not
		*/
		bool operator!=(const LILIString &other) const;

		//! Compare with other string.
		/*!
		  \param other the string to be compared.
		  \return true if they are equal, false if not
		*/
		bool operator==(const LILIString &other) const;

		//! Find the position of a character in a string.
		/*!
		  \param character the character to be searched.
		  \param pos the search starts at the position pos in the string.
		  \return the position of the first occurence of the character, -1 if the character was not found.
		*/
		int find(const char character,int pos = 0) const;

		//! Find the position of a string in a string.
		/*!
		  \param str the string to be searched.
		  \param pos the search starts at the position pos in the string.
		  \return the position of the first occurence of the string, -1 if the string was not found.
		*/
		int find(const char *str,int pos = 0) const;

		//! Find the position of a string in a string.
		/*!
		  \param other the string to be searched.
		  \param pos the search starts at the position pos in the string.
		  \return the position of the first occurence of the string, -1 if the string was not found.
		*/
		int find(const LILIString &other,int pos = 0) const;

		//! Find the last position of a character in a string.
		/*!
		  The search begins at the end of the string.
		  \param character the character to be searched.
		  \return the position of the last occurence of the character, -1 if the character was not found.
		*/
		int findLastOf(const char character) const;

		//! Extract a sub string.
		/*!
		  \param pos the extractions begins at the position pos in the string.
		  \param length the number of characters to be extracted, -1 if the string until the end will be extracted.
		  \return the substring, empty if paramaeters are out of range.
		*/
		LILIString substr(int pos,int length = -1) const;

	private:

		char *text;

};

#endif /* LILI_STRING */

/**********************************************************************/
// LILIBytes
/**********************************************************************/

#ifdef LILI_BYTES

//!  Manages byte arrays in memory. 
/*!
  This class provides mechanisms to manage byte arrays in RAM.
*/
class LILIBytes
{
	public:

		//! DefaultConstructor.
		/*!
		  Create empty byte array.
		*/
		LILIBytes();

		//! Copy constructor
		/*!
		  Copy existing byte array
		  \param array bytes to be copied.
		*/
		LILIBytes(const LILIBytes &array);

		//! Constructor to fill memory object.
		/*!
		  \param data the pattern with which the byte array is filled
		  \param length the number of bytes to be generated.
		*/
		LILIBytes(unsigned char data,int length);

		//! Constructor to copy bytes from memory.
		/*!
		  \param data the pointer to the memory.
		  \param length the number of bytes to be copied.
		*/
		LILIBytes(const unsigned char *data,int length);

		//! Destructor.
		/*!
		  This frees the reserved memory.
		*/
		~LILIBytes();

		//! Returns the number of bytes in the byte array.
		/*!
		  \return the number of bytes in object.
		*/
		int size() const;

		//! Returns if byte array contains some data.
		/*!
		  \return true = empty, false = contains some data.
		*/
		bool empty() const;

		//! Removes all data form byte array.
		void clear();

		//! This returns the pointer of the byte array in memory
		/*!
		  \return pointer to the byte array in memory.
		*/
		unsigned char* ptr() const;

		//! Change the size of the byte array.
		/*!
		  If the current size is bigger, data is removed from the end.
		  If the current size is smaller, new bytes are added but not initialised.
		  \param length the target size of the byte array.
		*/
		void resize(int length);

		//! Create a byte array with randome data.
		/*!
		  \param length the target size of the byte array.
		*/
		void randomise(int length);

		//! Copy data from another byte array.
		/*!
		  \param array the byte array to be copied.
		*/
		void setData(const LILIBytes &array);

		//! Create byte array with a fixed pattern
		/*!
		  \param data the pattern with which byte array is filled.
		  \param length the length of the new byte array.
		*/
		void setData(unsigned char data,int length);

		//! Copy data from memory.
		/*!
		  \param data pointer to the memory.
		  \param length The new length of the byte array.
		*/
		void setData(const unsigned char *data,int length);

		//! Copy operator.
		/*!
		  \param array the bytes to be copied.
		  \return pointer to itself
		*/
		LILIBytes& operator = (const LILIBytes &array);

		//! Compare operator.
		/*!
		  \param array the bytes to be compared.
		  \return true if equal, else false
		*/
		bool operator == (const LILIBytes &array);

		//! Compare operator.
		/*!
		  \param array the bytes to be compared.
		  \return true if equal, else false
		*/
		bool operator != (const LILIBytes &array);

		//! Operator to append data from another byte array.
		/*!
		  \param array the byte array to be appended.
		  \return pointer to itself
		*/
		LILIBytes& operator += (const LILIBytes &array);

		//! Append data from another byte array.
		/*!
		  \param array the byte array to be appended.
		*/
		void append(const LILIBytes &array);

		//! Returns the byte array as string in hex.
		/*!
		  \return the string in hex.
		*/
		LILIString toString();

	protected:

	private:

		unsigned char *data;

		int length;

};

#endif /* LILI_BYTES */

/**********************************************************************/
// LILILog
/**********************************************************************/

#ifdef LILI_LOG

//!  Logging information and errors of other LILI classes or applications using LILI. 
/*!
  This class provides a logging facility, to log information or errors of of operation.
  The current implementation provides only logging to the console output.
 
  The log level up to log messages are shown can be set.
    Different log levels can be used for each log message.  

  A global instance with the name lililog is created, which is used by all LILI classes.
  This instance can also be used by applications.
  
  \todo Provide logging interface to file.
  \todo Provide logging interface to syslog.
  \todo Provide logging interface to DLT.
*/
class LILILog
{
	public:

		//! The constructor.
		/*!
		*/
    	LILILog();

		//! The destructor.
		/*!
		*/		
		~LILILog();
		
		//! The different log levels of a log message.
		/*!
		*/		
		typedef enum { fatal,error,warn,info,debug,verbose } Level;

		//! Writing a log message into the log.
		/*!
		  \param level the level of the log level. See Level.
		  \param text the log level itself.
		  \sa Level
		*/
		void log(Level level,LILIString text);

		//! Set the log level, until logs are shown.
		/*!
		  \param loglevel log message up to this level are shown. See Level.
		  \sa Level
		*/
		void setLogLevel(Level loglevel);
		
		//! Get the log level, until logs are shown.
		/*!
		  \return up to this log level messages are shown.
		  \sa Level
		*/
		Level getLogLevel();

	protected:
	private:
	
		Level loglevel;

};

extern LILILog lililog;

#endif /* LILI_LOG */

/**********************************************************************/
// LILIArguments
/**********************************************************************/

#ifdef LILI_ARGUMENTS

//!  Parsing of an existing ini style configuration file. 
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  */
class LILIArguments
{
	public:

		//! The constructor.
		/*!
		*/
		LILIArguments();

		//! The destructor.
		/*!
		*/		
		~LILIArguments();
		
		//! This function parses a complete argument list.
		/*!
		  The list is provided by main function.
		  \param argc the number of arguments.
		  \param argv the arguments.
		  \return LILI_ERROR or LILI_OK.
		*/
		int parse(int argc, char *argv[]);
		
		//! This virtual function is called when an option as argument is detected.
		/*!
		  Options are something like "-v" or "--version"
		  \param option the name of the option.
		  \return LILI_ERROR or LILI_OK.
		*/
		virtual int parseOption(LILIString &option) = 0;

		//! This virtual function is called when an option with parameter as argument is detected.
		/*!
		  Options are something like "-value=10" or "--value=10"
		  \param option the name of the option.
		  \param parameter the value of the optionn.
		  \return LILI_ERROR or LILI_OK.
		*/
		virtual int parseOptionWithParamater(LILIString &option,LILIString &parameter) = 0;

		//! This virtual function is called when a command is detected.
		/*!
		  Commands are arguments without starting with - or --
		  \param argument the command.
		  \return LILI_ERROR or LILI_OK.
		*/
		virtual int parseArgument(LILIString &argument) = 0;
		
		//! Virtual functions which should display the version information.
		/*!
		  Is called when the arguments "-v" or "--version" are detected.
		*/
		virtual void printVersion() = 0;

		//! Virtual functions which should display the usage information.
		/*!
		  Is called when the arguments are wrong or contains no commands.
		*/
		virtual void printUsage() = 0;

	protected:

	private:

		//! Internal function.
		int parseInternalOption(LILIString option);

		//! Internal function.
		int parseInternalOptionWithParamater(LILIString option,LILIString parameter);

		//! Internal function.
		int parseInternalArgument(LILIString argument);

};

#endif /* LILI_ARGUMENTS */

/**********************************************************************/
// LILIConfiguration
/**********************************************************************/

#ifdef LILI_CONFIGURATION

//!  Parsing of an existing ini style configuration file. 
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  */
class LILIConfiguration
{
public:

		//! The constructor.
		/*!
		*/
		LILIConfiguration();

		//! The destructor.
		/*!
		*/		
		~LILIConfiguration();

		//! This function parses a complete .
		/*!
		  \param filename the filename of the configuration file to be parsed.
		  \return LILI_ERROR or LILI_OK.
		*/
		int parse(const LILIString &filename);

		//! This virtual function is called whenever a configuration value is detected.
		/*!
		  \param token the name of the configuration parameter
		  \param value the value of the configuration parameter.
		  \return LILI_ERROR or LILI_OK.
		*/
		virtual int parseToken(LILIString token,LILIString value) = 0;

protected:

private:

	LILIString filename;

};

#endif /* LILI_CONFIGURATION */

/**********************************************************************/
// LILISelect
/**********************************************************************/

#ifdef LILI_SELECT

//!  Provides the select() mechanism. 
/*!
  Select provides a mechanism to block a daemon, until one or more events happen in several file descriptors.
  This prevents to create one thread for each commuication channel. One central main loop can be used.
  
  \todo Add also write events.
*/
class LILISelect
{
	public:
	
		//! The constructor.
		/*!
		*/
		LILISelect();

		//! The destructor.
		/*!
		*/		
		~LILISelect();

		//! This is the main function of the select mechanism.
		/*!
		  This function call blocks until it receives at least one event.
		  \param mseconds the time until the blocking call is stopped. 0 if it blocks forever.
		  \return LILI_ERROR or LILI_OK.
		*/
		int select(unsigned int mseconds);

		//! Add a file desktiptor to the select list.
		/*!
		  \param fd the file descriptor to be added.
		*/
		void addFd(int fd);

		//! Remove a file descriptor from the select list.
		/*!
		  \param fd the file descriptor to be removed.
		*/
		void removeFd(int fd);
		
		//! Get the first file descriptor, which received an event.
		/*!
		  \return the first file descriptor, or -1 if no further file descriptor received an event.
		*/
		int first();

		//! Get the next file descriptor, which received an event.
		/*!
		  \return the next file decsriptor, or -1 if no further file descriptor received an event.
		*/
		int next();
		
	protected:
	private:
	
		fd_set master;        /**< master set of handles */
		fd_set read_fds;      /**< read set of handles */	
		int fdmax;            /**< highest number of used handles */
		int i;
};

#endif /* LILI_SELECT */

/**********************************************************************/
// LILITimer
/**********************************************************************/

#ifdef LILI_TIMER

//!  Timer object in milliseconds resolution 
/*!
  Timer used to measure time in milliseconds.
*/
class LILITimer
{
	public:

		//! Default constructor.
		/*!
		  Timer is set to invalid.
		*/
		LILITimer();

		//! Copy constructor.
		/*!
		  \param timer Timer to be copied.
		*/
		LILITimer(const LILITimer &timer);

		//! Constructor provides milliseconds
		/*!
		  Start timer.
		  \param mseconds Milliseconds after which timer is elapsed.
		*/
		LILITimer(unsigned int mseconds);

		//! Destructor.
		~LILITimer();
		
		//! Start the timer.
		/*!
		  \param mseconds The amount of milliseconds the timer runs.
		*/
		void start(unsigned int mseconds);

		//! Clears and stops timer.
		void clear();
		
		//! Check if timer is still running.
		/*!
		  \return true = time is elapsed, false = timer is still running.
		*/
		bool elapsed();

		//! The number of milliseconds the timer is still running.
		/*!
		  \return time in milliseconds the timer still runs.
		*/
		unsigned int togo();
	
		//! Copy an existing timer.
		/*!
		  \param timer The timer to be copied.
		  \return pointer to itself.
		*/
		LILITimer& operator = (const LILITimer &timer);
	
	protected:

		//! Returns the current number of ticks the PC is running.
		/*!
		  \return number of ticks.
		*/
		unsigned int ticks();

	private:
	
		unsigned int msecondsStart;
		
		unsigned int msecondsFinish;

};

#endif /* LILI_TIMER */

/**********************************************************************/
// LILISocket
/**********************************************************************/

#ifdef LILI_SOCKET

//!  Handle TCP/IP Socket connections for server and clients. 
/*!
  Manage socket conntecions and recv and send data.
*/
class LILISocket
{
	public:

		//! Default constructor.
		LILISocket();

		//! Constructor with existing handle.
		/*!
		  \param handle handle to be used.
		*/
		LILISocket(int handle);

		//! Constructor which copies existing socket.
		/*!
		  \param socket socket to be copied.
		*/
		LILISocket(const LILISocket &socket);

		//! Destructor
		~LILISocket();
		
		//! Initialiase socket as server.
		/*!
		  \param port The port the server waits for incoming connections.
		  \return LILI_ERROR or LILI_OK.
		*/
		int initServer(int port);

		//! Initialise socket as client.
		/*!
		  \return LILI_ERROR or LILI_OK.
		*/
		int initClient();
		
		//! Client connects to a server.
		/*!
		  \param address the directory path to be copied.
		  \param address the directory path to be copied.
		  \return LILI_ERROR or LILI_OK.
		*/
		int connect(const LILIString &address,int port);

		//! Accept connection of a listening server.
		/*!
		  \return The new socket object contains the new accepted connection.
		*/
		LILISocket* accept();
		
		//! Recv data from a socket
		/*!
		  \param data contains the received data after function call.
		  \return LILI_ERROR or LILI_OK.
		*/
		int recv(LILIBytes &data);

		//! Send some bytes to the opened socket.
		/*!
		  \param data the data to be sent.
		  \return LILI_ERROR or LILI_OK.
		*/
		int	send(const LILIBytes &data);
		
		//! Close the socket connection.
		void close();
		
		//! Get the file descriptor of the current socket connection.
		/*!
		  \return file descriptor.
		*/
		int getHandle();
		
	protected:

	private:

		int handle;

};

#endif /* LILI_SOCKET */

/**********************************************************************/
// LILITime
/**********************************************************************/

#ifdef LILI_TIME

//!  Manage the current time and provides time string. 
/*!
  Get the current time in seconds.
  Get text string from time.
*/
class LILITime
{
	public:

		//! Default constructor.
		/*!
		  Sets the object to the current time.
		*/
		LILITime();

		//! Constructo sets time in seconds.
		/*!
		  \param seconds the seconds to be set.
		*/
		LILITime(time_t seconds);

		//! Copy constructor.
		/*!
		  \param tim the existent time object to be copied.
		*/
		LILITime(const LILITime& tim);

		//! Destructor
		~LILITime();

		//! Copy operator.
		/*!
		  \param tim copy another time object.
		  \return pointer to itself.
		*/
		LILITime& operator=(const LILITime &tim);

		//! Set the time to current time.
		/*!
		  \return pointer to itself.
		*/
		LILITime&  current();

		//! Export time to string.
		/*!
		  \param fmt the directory path to be copied.
		  \return The returned string.
		*/
		LILIString toString(const char *fmt = "%Y%m%d%H%M%S");
		
		//! Get the seconds of the time.
		/*!
		  \return the seconds.
		*/
		time_t getSeconds();
		
	protected:

	private:

		time_t seconds;

};

#endif /* LILI_TIME */

/**********************************************************************/
// LILIDir
/**********************************************************************/

#ifdef LILI_DIR

//!  Provides a class to manage and read directories. 
/*!
  Get all files and subdirectory of a directory.
  Handle directory pathes.
  
  \todo Add also write events.
*/
class LILIDir
{
	public:
		//! This is the type of directory entry.
		typedef enum { File,Dir,Unknown } Type;
	
		//! Default constructor.
		LILIDir();

		//! Constructor which sets directly a path.
		/*!
		  \param path the path to be set.
		*/
		LILIDir(const LILIString &path);

		//! Constructor to copy an existing directory path.
		/*!
		  \param dir Another directory object.
		*/
		LILIDir(const LILIDir &dir);

		//! Destructor.
		~LILIDir();
		
		//! Set the path of the directory.
		/*!
		  \param _path the new directory path.
		*/
		void setPath(LILIString _path);

		//! Get the current path.
		/*!
		  \return the current set path.
		*/
		LILIString getPath();

		//! Set the path to the current working directory.
		void setPathCwd();
		
		//! Operator to copy two directories pathes.
		/*!
		  \param dir the directory path to be copied.
		  \return pointer to itself.
		*/
		LILIDir& operator=(const LILIDir &dir);

		//! This function selects the first directory entry of the set path.
		/*!
		  \return LILI_ERROR = path invalid or no furtner directory entries, LILI_OK = first entry selected.
		*/
		int first();

		//! This function selects the next directory entry of the set path.
		/*!
		  \return LILI_ERROR = path invalid or no furtner directory entries, LILI_OK = next entry selected.
		*/
		int next();

		//! Close the directory search.
		void close();

		//! Create a new directory with the name of the selected path.
		/*!
		  \return LILI_ERROR or LILI_OK.
		*/
		int createDirectory();

		//! Remove file selected by path.
		/*!
		  The path includes also the filename.
		  \return LILI_ERROR or LILI_OK.
		*/
		int removeFilename();

		//! This function tries to find a subdirectory in all higher level directories.
		/*!
		  It searches form low level to higher level directories.
		  \param dirname the sub directory to be searched for.
		  \return the name of the directory including the directory name dirname.
		*/
		LILIString findSubdir(LILIString dirname);

		//! Checks if the selected directory path exists.
		/*!
		  \return true = exists, false = not exists.
		*/
		bool exist();
		
		//! This returns the filename of the current selected filepath.
		/*!
		  \return the filename.
		*/
		LILIString getFilename();

		//! This returns the type of the current selected filepath.
		/*!
		  Possible types are directory or file.
		  \see Type
		  \return the filename.
		*/
		Type getType();
		
	protected:

	private:
	
		LILIString path;
		
		DIR *dp;
		
		LILIString filename;
		
		Type type;
};

#endif /* LILI_DIR */

#endif /* LILI_H */


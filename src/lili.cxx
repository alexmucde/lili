/*
LILI - Linux library for easy writing of linux daemons in C++.
Copyright (C) 2012  Alexander Wenzel

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <lili.h>

/**********************************************************************/
// LILIString
/**********************************************************************/

#ifdef LILI_STRING

LILIString::LILIString()
{
	text = new char[1];
	text[0] = 0;
}

LILIString::LILIString(const char * str)
{
	text = new char[strlen(str)+1];
	
	strcpy(text,str);
}

LILIString::LILIString(const LILIBytes &bytes,int offset)
{
	if(offset>=bytes.size()) {
		// error empty string
		text = new char[1];
		text[0] = 0;
	}
	else {
		text = new char[bytes.size()-offset+1];
		strncpy(text,(char*)bytes.ptr()+offset,bytes.size()-offset);
		text[bytes.size()-offset] = 0;
	}
}

LILIString::LILIString(const LILIString &other)
{
	text = new char[strlen(other.text)+1];
	
	strcpy(text,other.text);
}

LILIString::~LILIString()
{
	free(text);
}

void LILIString::clear()
{
	free(text);

	text = new char[1];
	text[0] = 0;
}

int LILIString::size() const
{
	return strlen(text);
}

bool LILIString::empty() const
{
	return (strlen(text)==0);
}

char* LILIString::ptr() const
{
	return text;
}

LILIString::operator char*() const
{
	return text;
}

LILIString& LILIString::append(const char *str)
{
	char *ptr;
	
	ptr = new char[strlen(str)+strlen(text)+2];
	
	strcpy(ptr,text);
	
	strcat(ptr,str);
	
	delete text;
	
	text = ptr;

	return *this;	
}

LILIString& LILIString::append(const LILIString &other)
{
	return append(other.text);
}

LILIString&  LILIString::operator+=(const char *str)
{
	return append(str);
}

LILIString& LILIString::operator+=(const LILIString &other)
{
	return append(other);
}

LILIString LILIString::operator+(const char *str)
{
	return LILIString(*this).append(str);
}

LILIString LILIString::operator+(const LILIString &other)
{
	return LILIString(*this).append(other);
}

LILIString&  LILIString::operator=(const char *str)
{
	text = new char[strlen(str)+1];	
	strcpy(text,str);
}

LILIString&  LILIString::operator=(const LILIString &other)
{
	*this = other.text;
}

bool LILIString::equal(const char *str) const
{
	return (strcmp(text,str)==0);
}

bool LILIString::operator!=(const LILIString &other) const
{
	return !equal(other.text);	
}

bool LILIString::operator==(const LILIString &other) const
{
	return equal(other.text);	
}

bool LILIString::operator!=(const char *str) const
{
	return !equal(str);
}

bool LILIString::operator==(const char *str) const
{
	return equal(str);
}

int LILIString::find(const char character,int pos) const
{	
	if(pos<0 || pos>=strlen(text))
		return -1;

	char *ptr = strchr(text+pos,character);
	
	if(!ptr)
		return -1;
		
	return (ptr-text);
}

int LILIString::find(const char *str,int pos) const
{
	if(pos<0 || pos>=strlen(text))
		return -1;

	char *ptr = strstr(text+pos,str);

	if(!ptr)
		return -1;
		
	return (ptr-text);
}

int LILIString::find(const LILIString &other,int pos) const
{
	return find(other.text,pos);
}

int LILIString::findLastOf(const char character) const
{
	for(int num=strlen(text);num>=0;num--) {
		if(text[num]==character)
			return num;
	}

	return -1;
}

LILIString LILIString::substr(int pos,int length) const
{		
	LILIString dest;

	if(pos<0 || pos>=strlen(text))
		return dest;

	delete dest.text;

	if(length == -1) {
		dest.text = new char[strlen(text)-pos];
		strncpy(dest.text,text+pos,strlen(text)-pos);
		dest.text[strlen(text)-pos] = 0 ;
	}
	else {
		dest.text = new char[length+1];
		strncpy(dest.text,text+pos,length);
		dest.text[length] = 0;		
	}
	
	return dest;
}

#endif /* LILI_STRING */

/**********************************************************************/
// LILIBytes
/**********************************************************************/

#ifdef LILI_BYTES

LILIBytes::LILIBytes()
{
	data = (unsigned char *)malloc(0);
	length = 0;
}

LILIBytes::LILIBytes(const LILIBytes &array)
{
	data = (unsigned char *)malloc(array.length);
	memcpy(data,array.data,array.length);
	length = array.length;
}

LILIBytes::LILIBytes(unsigned char _data,int _length)
{
	data = (unsigned char *)malloc(_length);
	memset(data,_data,_length);
	length = _length;
}

LILIBytes::LILIBytes(const unsigned char *_data,int _length)
{
	data = (unsigned char *)malloc(_length);
	memcpy(data,_data,_length);
	length = _length;
}

LILIBytes::~LILIBytes()
{
	free(data);
}

int LILIBytes::size() const
{
	return length;
}

unsigned char* LILIBytes::ptr() const
{
	return data;
}

bool LILIBytes::empty() const
{
	return length>0?false:true;
}

void LILIBytes::clear()
{
	free(data);
	data = (unsigned char *)malloc(0);
	length = 0;	
}

void LILIBytes::resize(int _length)
{
	int oldlength = length;
	unsigned char *olddata = data;
	data = (unsigned char *)malloc(_length);
	memcpy(data,olddata,_length>oldlength?oldlength:_length);
	free(olddata);
	length = _length;
}

void LILIBytes::randomise(int _length)
{
	delete data;
	data = (unsigned char *)malloc(_length);
	length = _length;

	srand ( time(NULL) );
	
	for(int n=0; n<length; n++) {
		data[n] = rand()%256;	
	}
}

void LILIBytes::setData(const LILIBytes &array)
{
	free(data);
	data = (unsigned char *)malloc(array.length);
	memcpy(data,array.data,array.length);
	length = array.length;
}

void LILIBytes::setData(unsigned char _data,int _length)
{
	free(data);
	data = (unsigned char *)malloc(_length);
	memset(data,_data,_length);
	length = _length;
}

void LILIBytes::setData(const unsigned char *_data,int _length)
{
	free(data);
	data = (unsigned char *)malloc(_length);
	memcpy(data,_data,_length);
	length = _length;
}

LILIBytes& LILIBytes::operator = (const LILIBytes &array)
{
	setData(array);
	
	return *this;
}

bool LILIBytes::operator == (const LILIBytes &array)
{
	if(length!=array.length)
		return false;
	
	if(length==0)
		return true;
	
	return (memcmp(data,array.data,length)==0);
}

bool LILIBytes::operator != (const LILIBytes &array)
{
	return !(*this==array);
}

LILIBytes& LILIBytes::operator += (const LILIBytes &array)
{
	append(array);
	
	return *this;
}

void LILIBytes::append(const LILIBytes &array)
{
	int oldlength = length;
	unsigned char *olddata = data;
	data = (unsigned char *)malloc(oldlength+array.length);
	memcpy(data,olddata,oldlength);
	memcpy(data+oldlength,array.data,array.length);
	free(olddata);
	length = oldlength+array.length;	
}

LILIString LILIBytes::toString()
{
	char *str;
	LILIString output;
	
	if(length==0)
		return LILIString();
	
	str = (char*)malloc(length*2+1);

	for(int n=0; n<length; n++)
			sprintf(str+n*2,"%02x", data[n]);	

	output.append(str);

	free(str);

	return output;
}

#endif /* LILI_BYTES */

/**********************************************************************/
// LILILog
/**********************************************************************/

#ifdef LILI_LOG

LILILog lililog;

LILILog::LILILog()
{
	loglevel = LILILog::info;
}

LILILog::~LILILog()
{
	
}

void LILILog::log(Level level,LILIString text)
{
	/* do not show logs which are higher then the current log level */
	if(level>loglevel)
		return;
	
	/* output current time */
	cout << LILITime().toString("%Y/%m/%d %H:%M:%S ");
		
	/* output log level */
	switch(level)
	{
		case LILILog::fatal:
			cout << "fatal";
			break;
		case LILILog::error:
			cout << "error";
			break;
		case LILILog::warn:
			cout << "warn";
			break;
		case LILILog::info:
			cout << "info";
			break;
		case LILILog::debug:
			cout << "debug";
			break;
		case LILILog::verbose:
			cout << "verbose";
			break;
	}

	/* output log message */
	cout << ": " << text << endl;
}

void LILILog::setLogLevel(Level _loglevel)
{
	loglevel = _loglevel;
}

LILILog::Level LILILog::getLogLevel()
{
	return loglevel;
}

#endif /* LILI_LOG */
		
/**********************************************************************/
// LILISelect
/**********************************************************************/

#ifdef LILI_SELECT

LILISelect::LILISelect()
{
	fdmax = 0;
	i = 0;
}

LILISelect::~LILISelect()
{
    FD_ZERO(&(master));
    FD_ZERO(&(read_fds));	
}

int LILISelect::select(unsigned int mseconds)
{
    struct timeval tv;

	/* Wait up to one second. */
	tv.tv_sec = 0;
	tv.tv_usec = mseconds * 1000;

	/* wait for		int findLastOf(const char character) const;
 events form all FIFO and sockets */
	read_fds = master;
	if (::select(fdmax+1, &(read_fds), NULL, NULL,&tv) == -1)
		return LILI_ERROR;
		
	return LILI_OK;
}

void LILISelect::addFd(int fd)
{
	FD_SET(fd, &(master));
	if (fd > fdmax)
	{
		fdmax = fd;
	}									
}

void LILISelect::removeFd(int fd)
{
    FD_CLR(fd, &(master));
}

int LILISelect::first()
{
	i = 0;
	
	return next();
}

int LILISelect::next()
{
	int selected;
	
	while(i<=fdmax) {
		if (FD_ISSET(i, &(read_fds))) {
			selected = i;
			i++;
			return selected;
		}
		i++;
	}
	return -1;
}

#endif /* LILI_SELECT */
		
/**********************************************************************/
// LILITimer
/**********************************************************************/

#ifdef LILI_TIMER

LILITimer::LILITimer()
{
	clear();
}

LILITimer::LILITimer(const LILITimer &timer)
{
	*this = timer;
}

LILITimer::LILITimer(unsigned int mseconds)
{
	start(mseconds);
}

LILITimer::~LILITimer()
{
}

void LILITimer::start(unsigned int mseconds)
{
	msecondsStart = ticks();
	msecondsFinish = msecondsStart+mseconds;
}

void LILITimer::clear()
{
	msecondsStart = 0;
	msecondsFinish = 0;	
}

bool LILITimer::elapsed()
{
	return (ticks()>=msecondsFinish);
}

unsigned int LILITimer::togo()
{
	return (msecondsFinish-ticks());
}

LILITimer& LILITimer::operator = (const LILITimer &timer)
{
	msecondsStart = timer.msecondsStart;
	msecondsFinish = timer.msecondsFinish;
	
	return *this;
}

unsigned int LILITimer::ticks()
{
    struct timespec ts;

    if (clock_gettime(CLOCK_MONOTONIC,&ts)==0)
    {
        return (uint32_t)((((ts.tv_sec*1000000)+(ts.tv_nsec/1000)))/1000); // in 1 ms = 1000 us
    }
    else
    {
        return 0;
    }	
}

#endif /* LILI_TIMER */

/**********************************************************************/
// LILISocket
/**********************************************************************/

#ifdef LILI_SOCKET

LILISocket::LILISocket()
{
	handle = 0;
}

LILISocket::LILISocket(int _handle)
{
	handle = _handle;
}

LILISocket::LILISocket(const LILISocket &socket)
{
	handle = socket.handle;
}

LILISocket::~LILISocket()
{
	if(handle>0)
		::close(handle);
}

int LILISocket::initServer(int port)
{
    int yes = 1;
    struct sockaddr_in servAddr;

    if ((handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        return LILI_ERROR;

    setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family      = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port        = htons(port);

    if (bind(handle, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
        return LILI_ERROR;

    if (listen(handle, 3) < 0)
        return LILI_ERROR;

	return LILI_OK;
}

int LILISocket::initClient()
{
	return LILI_OK;
}

int LILISocket::connect(const LILIString &address,int port)
{
	struct sockaddr_in servAddr;
	struct hostent *host;            /* Structure containing host information */

	/* open socket */
	if ((handle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		return LILI_ERROR;

	if ((host = (struct hostent*) gethostbyname(address)) == 0)
		return LILI_ERROR;

	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family      = AF_INET;
	servAddr.sin_addr.s_addr = inet_addr(inet_ntoa(*( struct in_addr*)( host -> h_addr_list[0])));
	servAddr.sin_port        = htons(port);

	if (::connect(handle, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
		return LILI_ERROR;
	
	return LILI_OK;
}

LILISocket* LILISocket::accept()
{
    socklen_t cli_size;
    struct sockaddr cli;
    int in_sock;

    /* event from TCP server socket, new connection */
    cli_size = sizeof(cli);
    if ((in_sock  = ::accept(handle,&cli, &cli_size)) < 0)
        return NULL ;

	return new LILISocket(in_sock);
}

int LILISocket::recv(LILIBytes &data)
{
	unsigned char buf[1024];
	int bytes;
	
	bytes = ::recv(handle,buf,1024,0);
	
	if(bytes<=0)
		return LILI_ERROR;	
	
	data.setData(buf,bytes);
	
	return LILI_OK;
}

int	LILISocket::send(const LILIBytes &data)
{
	int bytes;
	
	bytes = ::send(handle, data.ptr(),data.size(),0);
	
	if(bytes<0)
		return LILI_ERROR;	
	
	return LILI_OK;
}

void LILISocket::close()
{
}

int LILISocket::getHandle()
{ 
	return handle;
}

#endif /* LILI_SOCKET */

/**********************************************************************/
// LILITime
/**********************************************************************/

#ifdef LILI_TIME

LILITime::LILITime()
{
	current();
}

LILITime::LILITime(time_t _seconds)
{
	seconds = _seconds;
}

LILITime::LILITime(const LILITime& tim)
{
	*this = tim;
}

LILITime::~LILITime()
{

}

LILITime& LILITime::operator=(const LILITime &tim)
{
	seconds = tim.seconds;
}

LILITime&  LILITime::current()
{
	time ( &seconds );
}

LILIString LILITime::toString(const char *fmt)
{
	struct tm * timeinfo;
	char buffer [80];

	timeinfo = localtime ( &seconds );

	strftime (buffer,80,fmt,timeinfo);
	
	return LILIString(buffer);
}

time_t LILITime::getSeconds()
{ 
	return seconds;
}

#endif /* LILI_TIME */

/**********************************************************************/
// LILIDir
/**********************************************************************/

#ifdef LILI_DIR

LILIDir::LILIDir()
{
	dp = NULL;
}

LILIDir::LILIDir(const LILIString &_path)
{
	path = _path;
	dp = NULL;
}

LILIDir::LILIDir(const LILIDir &dir)
{
	*this = dir;
	dp = NULL;
}

LILIDir::~LILIDir()
{
	close();
}

LILIDir& LILIDir::operator=(const LILIDir &dir)
{
	path = dir.path;
	close();
}

int LILIDir::first()
{
	/* close if last still open */
	close();

    if((dp  = opendir(path)) == NULL)
        return LILI_ERROR;

	return next();
}

int LILIDir::next()
{
    struct dirent *dirp;
    struct stat sb;

	do {
		if((dirp = readdir(dp)) == NULL) {
			filename.clear();
			return LILI_ERROR;
		}
					
		filename = LILIString(dirp->d_name);
	} while(filename == "." || filename == "..");

	lstat((path + "/" + filename),&sb);

	if((sb.st_mode & S_IFDIR)) {
		type = LILIDir::Dir;
	}
	else if((sb.st_mode & S_IFREG)) {
		type = LILIDir::File;		
	}
	else
	{
		type = LILIDir::Unknown;		
	}

	return LILI_OK;
}

void LILIDir::close()
{
	if(dp)
		closedir(dp);

	dp = NULL;
}

void LILIDir::setPathCwd()
{	
	char _path[MAXPATHLEN];
	getcwd(_path, MAXPATHLEN);

	path = LILIString(_path);
}
		
int LILIDir::createDirectory()
{
    struct stat sb;
    int pos = 1;
    int ret;

	pos = path.find('/',pos);
	while(pos!=string::npos) {
		ret = lstat(path.substr(0,pos),&sb);		
		if(ret) {
			if(mkdir(path.substr(0,pos),S_IRWXU)!=0)
				return LILI_ERROR;
		}	
		pos = path.find('/',++pos);
	}

	ret = lstat(path,&sb);		
	if(ret || !(sb.st_mode & S_IFDIR)) {
		if(mkdir(path,S_IRWXU)!=0) {
			return LILI_ERROR;
		}
	}	
	
	return LILI_OK;
}

int LILIDir::removeFilename()
{
    int pos = 1;

	pos = path.findLastOf('/');
	if(pos!=string::npos) {
		path = path.substr(0,pos);
		return LILI_OK;	
	}
	
	return LILI_ERROR;	
}

bool LILIDir::exist()
{
    struct stat sb;
    int pos = 1;
    int ret;

	ret = lstat(path,&sb);
	if(ret) {
		return false;
	}	
	if(sb.st_mode & S_IFDIR) {
		return true;
	}	
	if(sb.st_mode & S_IFLNK) {
		return true;
	}	
	return false;
}

LILIString LILIDir::findSubdir(LILIString dirname)
{
    struct stat sb;
    int pos = 0;
    int ret;

	ret = lstat((path+"/"+dirname),&sb);		
	if(ret==0 && (sb.st_mode & S_IFDIR)) {
		return path;
	}	

	pos = path.find('/',pos);
	while(pos!=string::npos) {
		ret = lstat((path.substr(0,pos)+"/"+dirname),&sb);		
		if(ret==0 && (sb.st_mode & S_IFDIR)) {
			return path.substr(0,pos);
		}	
		pos = path.find('/',++pos);
	}
	
	return LILIString("");
}

void LILIDir::setPath(LILIString _path)
{
	path = _path;
}

LILIString LILIDir::getPath()
{
	return path;
}

LILIString LILIDir::getFilename()
{ 
	return filename;
}

LILIDir::Type LILIDir::getType()
{
	return type;
}

#endif /* LILI_DIR */

/**********************************************************************/
// LILIArguments
/**********************************************************************/

#ifdef LILI_ARGUMENTS

LILIArguments::LILIArguments() {
}

LILIArguments::~LILIArguments() {
}

int LILIArguments::parse(int argc, char *argv[]) {
	LILIString opt;
	int find;
	int ret;

	for(int num=1;num<argc;num++) {
		opt = LILIString(argv[num]);
		
		if(opt.find("--")==0) {
			if((find=opt.find("="))!=string::npos) {
				if((ret=parseInternalOptionWithParamater(opt.substr(0,find),opt.substr(find+1)))!=LILI_OK) {
					return ret;
				}
			}
			else {
				if((ret=parseInternalOption(opt))!=LILI_OK) {
					return ret;
				}
			}
		}		
		else if(opt.find("-")==0) {
			if((find=opt.find("="))!=string::npos) {
				if((ret=parseInternalOptionWithParamater(opt.substr(0,find),opt.substr(find+1)))!=LILI_OK) {
					return ret;
				}
			}
			else {
				if((ret=parseInternalOption(opt))!=LILI_OK) {
					return ret;
				}
			}
		}
		else {
			if((ret=parseInternalArgument(opt))!=LILI_OK) {
				return ret;
			}
		}
		
	}
	
	return LILI_OK;
}

int LILIArguments::parseInternalArgument(LILIString argument)
{
	int ret;

	ret = parseArgument(argument);

	if(ret<0) {
		lililog.log(LILILog::error,LILIString("Unknown argument ") + argument);
	}

	return ret;
}

int LILIArguments::parseInternalOption(LILIString option)
{
	int ret;

	if(option == "--version" || option == "-v") {
		printVersion();
		return LILI_EXIT;
	}

	if(option == "--help" || option == "-h") {
		printUsage();
		return LILI_EXIT;
	}
	
	ret = parseOption(option);

	if(ret<0) {
		lililog.log(LILILog::error,LILIString("Unknown option ") + option);
	}

	return ret;
}

int LILIArguments::parseInternalOptionWithParamater(LILIString option,LILIString parameter)
{
	int ret;

	ret = parseOptionWithParamater(option,parameter);

	if(ret<0) {
		lililog.log(LILILog::error,LILIString("Unknown option ") + option + " = " + parameter);
	}

	return ret;
}

#endif /* LILI_ARGUMENTS */

/**********************************************************************/
// LILIConfiguration
/**********************************************************************/

#ifdef LILI_CONFIGURATION

LILIConfiguration::LILIConfiguration()
{
}

LILIConfiguration::~LILIConfiguration()
{
}

int LILIConfiguration::parse(const LILIString &filename)
{
	FILE * pFile;
	char line[1024];
	char token[1024];
	char value[1024];
    char *pch;

	/* open configuration file */
	pFile = fopen (filename,"r");

	if (pFile!=NULL)
	{
		while(1)
		{
			/* fetch line from configuration file */
			if ( fgets (line , 1024 , pFile) != NULL )
			{
				  pch = strtok (line," =\r\n");
				  token[0]=0;
				  value[0]=0;
				  
				  while (pch != NULL)
				  {
					if(strcmp(pch,"#")==0)
						break;

					if(token[0]==0)
					{
						strncpy(token,pch,sizeof(token));
					}
					else
					{
						strncpy(value,pch,sizeof(value));
						break;
					}

					pch = strtok (NULL, " =\r\n");
				  }
				  
				  if(token[0] && value[0])
				  {
						/* parse arguments here */
						if(parseToken(LILIString(token),LILIString(value)) == LILI_ERROR)
						{
							lililog.log(LILILog::error,LILIString("Unknown option in configuration file ") + LILIString(token) + " = " + LILIString(value));
							return LILI_ERROR;
						}
				}
			}
			else
			{
				break;
			}
		}
		fclose (pFile);
	}
	else
	{
		lililog.log(LILILog::error,LILIString("Cannot open configuration file ") + filename);
		return LILI_ERROR;
	}	

	return LILI_OK;
}

#endif /* LILI_CONFOGURATION */
